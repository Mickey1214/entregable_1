package mis.entregables.back01_entregable.servicio;

import mis.entregables.back01_entregable.modelo.Producto;
import mis.entregables.back01_entregable.modelo.Usuario;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ProductoServicio {

    //Atributo privado
    private List<Producto> dataList = new ArrayList<Producto>();

    //Poblado de datos en variable
    public ProductoServicio() {
        dataList.add(new Producto(1, "producto 1", 100.50));
        dataList.add(new Producto(2, "producto 2", 150.00));
        dataList.add(new Producto(3, "producto 3", 100.00));
        dataList.add(new Producto(4, "producto 4", 50.75));
        dataList.add(new Producto(5, "producto 5", 120.00));
        List<Usuario> users = new ArrayList<>();
        users.add(new Usuario("1"));
        users.add(new Usuario("3"));
        users.add(new Usuario("5"));
        dataList.get(1).setUsuarios(users);
    }

    //Metodo que regresa toda la lista de los productos
    public List<Producto> getProductos() {
        return dataList;
    }

    //Metodo que regresa un producto en especifico
    public Producto getProducto(long index) throws IndexOutOfBoundsException {
        if(getIndex(index)>=0) {
            return dataList.get(getIndex(index));
        }
        return null;
    }

    // Get index
    public int getIndex(long index) throws IndexOutOfBoundsException {
        int i=0;
        while(i<dataList.size()) {
            if(dataList.get(i).getId() == index){
                return(i); }
            i++;
        }
        return -1;
    }

    // Agrega un producto
    public Producto addProducto(Producto newPro) {
        dataList.add(newPro);
        return newPro;
    }

    // UPDATE el producto y su indice
    public Producto updateProducto(int index, Producto newPro)
            throws IndexOutOfBoundsException {
        dataList.set(index, newPro);
        return dataList.get(index);
    }

    // DELETE el indice
    public void removeProducto(int index) throws IndexOutOfBoundsException {
        int pos = dataList.indexOf(dataList.get(index));
        dataList.remove(pos);
    }
}
