package mis.entregables.back01_entregable.modelo;

public class Usuario {
    //Atributos
    private String userId;

    //Constructor
    public Usuario(){}

    public Usuario(String userId){
        this.userId = userId;
    }

    //Getters
    public String getUserId(){
        return this.userId;
    }
}
