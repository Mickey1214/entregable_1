package mis.entregables.back01_entregable.modelo;

import java.util.List;

public class Producto {
    //Atributos
    private long id;
    private String descripcion;
    private double precio;
    //final List<Integer> idsProveedores = new ArrayList<>();
    private List<Usuario> usuarios;

    //Constructor
    public Producto(long id, String descripcion, double precio) {
        this.id = id;
        this.descripcion = descripcion;
        this.precio = precio;
//        this.usuarios = new ArrayList<Usuario>();
    }

    //Getters
    public long getId() {
        return id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public double getPrecio() {
        return precio;
    }

    public List<Usuario> getUsuarios() {
        return usuarios;
    }

    //Setters
    public void setId(long id) {
        this.id = id;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public void setUsuarios(List<Usuario> usuarios) {
        this.usuarios = usuarios;
    }
}
