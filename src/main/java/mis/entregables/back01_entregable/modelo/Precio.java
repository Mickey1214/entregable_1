package mis.entregables.back01_entregable.modelo;

public class Precio {
    //Atributos
    private long id;
    private double producto_precio;

    //Constructores
    public Precio() {
    }

    public Precio(double precio) {
        this.producto_precio = precio;
    }

    public double getPrecio() {
        return producto_precio;
    }

    public void setPrecio(double precio) {
        this.producto_precio = precio;
    }


}
