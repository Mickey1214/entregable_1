//Manejo de las peticiones HTTP
package mis.entregables.back01_entregable.controlador;

import mis.entregables.back01_entregable.modelo.Precio;
import mis.entregables.back01_entregable.modelo.Producto;
import mis.entregables.back01_entregable.servicio.ProductoServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
//Agregar ruta base de las peticiones
@RequestMapping("${url.base}")
public class ProductoControlador {

    @Autowired
    private ProductoServicio productoServicio;


    //Metodo/Peticion que regresa toda la lista de los productos
    @GetMapping("/productos")
    public List<Producto> getProductos() {
        return productoServicio.getProductos();
    }

    //Metodo/Peticion que regresa un producto en especifico
    @GetMapping("/productos/{id}")
    //Se usa PathVariable ya que se esta extrayendo el dato/valor de la uri
    public ResponseEntity getProductoId(@PathVariable int id) {
        Producto pr = productoServicio.getProducto(id);
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(pr);
    }

    //Metodo/Peticion que agrega un producto dentro de la lista de productos
    @PostMapping("/productos")
    public ResponseEntity<String> addProducto(@RequestBody Producto productoModel) {
        productoServicio.addProducto(productoModel);
        return new ResponseEntity<>("Producto creato satisfactoriamente!", HttpStatus.CREATED);
    }

    //Metodo/Peticion que modifica un producto si existe
    @PutMapping("/productos/{id}")
    public ResponseEntity updateProducto(@PathVariable int id,
                                         @RequestBody Producto productToUpdate) {
        Producto pr = productoServicio.getProducto(id);
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        productoServicio.updateProducto(id-1, productToUpdate);
        return new ResponseEntity<>("Producto actualizado correctamente.", HttpStatus.OK); // Buenas prácticas dicen que mejor enviar 204 No Content
    }

    //Metodo/Peticion que elimina un producto en especifico
    @DeleteMapping("/productos/{id}")
    public ResponseEntity deleteProducto(@PathVariable Integer id) {
        Producto pr = productoServicio.getProducto(id);
        if(pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        productoServicio.removeProducto(id - 1);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT); // status code 204-No Content
    }

    //Metodo/Peticion que modifica un solo elemento dentro del recurso
    @PatchMapping("/productos/{id}")
    public ResponseEntity patchPrecioProducto(
            @RequestBody Precio productoPrecioOnly, @PathVariable int id){
        Producto pr = productoServicio.getProducto(id);
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        pr.setPrecio(productoPrecioOnly.getPrecio());
        productoServicio.updateProducto(id-1, pr);
        return new ResponseEntity<>(pr, HttpStatus.OK);
    }

    @GetMapping("/productos/{id}/users")
    public ResponseEntity getProductIdUsers(@PathVariable int id){
        Producto pr = productoServicio.getProducto(id);
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        if (pr.getUsuarios()!=null)
            return ResponseEntity.ok(pr.getUsuarios());
        else
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
